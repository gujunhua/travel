# travel

# 全局安装 vue-cli
npm install --global vue-cli
# 创建一个基于 webpack 模板的项目
vue init webpack my-project
# 安装依赖
cd my-project
npm install
npm run dev

# 解决300ms点击事件延迟问题  在项目下 进行安装
npm install fastclick --save

# stylus 样式
npm install stylus --save
npm install stylus-loader --save

# 安装swiper插件
npm install vue-awesome-swiper@2.6.7 --save

# 安装axios插件
npm install axios --save

# 安装滚动插件 better-scroll
npm install better-scroll --save

# 安装 vuex
npm install vuex --save

# 手机白屏问题
npm install babel-polyfill --save

# 编译
npm run build

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

# 作者个人博客
http://www.gujunhua.vip
