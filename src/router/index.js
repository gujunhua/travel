import Vue from 'vue'
import Router from 'vue-router'
import City from '@/pages/city/City'
import Detail from '@/pages/detail/Detail'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home', // 路由起一个名字
      component: () => import('@/pages/home/Home') // 异步组件加载  这个使用必须是大型项目  小型项目无需这么做的
    },
    {
      path: '/city',
      name: 'City',
      component: City
    },
    {
      // :id动态路由
      path: '/detail/:id',
      name: 'Detail',
      component: Detail
    }
  ],
  scrollBehavior (to, from, savedPosition) {
    return { x: 0, y: 0 }
  }
})
