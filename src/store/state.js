// 使用localStorage最好使用try catch防止用户行为导致程序执行不了
let defaultCity = '上海'
try {
  if (localStorage.city) {
    defaultCity = localStorage.city
  }
} catch (e) {}

export default {
  city: defaultCity
}
