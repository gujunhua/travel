import Vue from 'vue'
import Vuex from 'vuex'
import state from './state'
import mutations from './mutations'

Vue.use(Vuex)

export default new Vuex.Store({
  // 键值一样 进一步简化 state: state --> state
  state,
  // 组件改变数据的
  actions: {
    changeCity (ctx, city) {
      ctx.commit('changeCity', city) // 执行的是mutations中的changeCity方法
    }
  },
  // action执行后调用这里的方法,改变数据
  mutations,
  getters: {
    doubleCity (state) {
      return state.city + ' ' + state.city
    }
  }
})
