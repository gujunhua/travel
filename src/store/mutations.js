export default {
  changeCity (state, city) {
    state.city = city
    // 浏览器本地存储 比cookie更好用
    try {
      localStorage.city = city
    } catch (e) {}
  }
}
